from Players.PlayerController import PlayerController

class HumanPlayer(PlayerController):
    """
    Creates human player, enabling human computer interaction through the console
    @param playerId: either 1 or 2
    @param gameN: N in a row required to win
    @param heuristic: the heuristic the player should use
    """
    def __init__(self, playerId, gameN, heuristic):
        super().__init__(playerId, gameN, heuristic)

    """
    Shows the human player the current board and asks them for their next move
    """
    def makeMove(self, board):
        print(board)
        print("Player ", "\nWhich column would you like to play in?")

        while True:
            inp = input()
            if inp == "exit":
                exit()
            try:
                column = int(inp)
                if column <= board.width and column > 0 and board.isValid(column-1):
                    break
                print("Please enter a valid column number, or 'exit' to stop.")
            except:
                print("Please enter a valid column number, or 'exit' to stop.")

        print("Selected column: ", column)

        self.moveCount += 1
        return column - 1