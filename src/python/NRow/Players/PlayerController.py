class PlayerController():
    """
    Creates human player, enabling human computer interaction through the console
    @param playerId: can take values 1 or 2 (0 = empty)
    @param gameN: N in a row required to win
    @param heuristic: the heuristic the player should use
    """
    def __init__(self, playerId, gameN, heuristic):
        self.playerId = playerId
        self.gameN = gameN
        self.heuristic = heuristic
        self.moveCount = 0

    """
    @return: The amount of times the heuristic was used to evaluate a boardstate
    """
    def getEvalCount(self):
        if self.heuristic is not None:
            return self.heuristic.getEvalCount()
        return None

    def getMoveCount(self):
        return self.moveCount

    """
    Gets a nice String representation for displaying the board
    """
    def __str__(self):
        if self.playerId == 2:
            return "O"
        return "X"

    """
    @param board: the current board
    @return: column integer the player chose
    """
    def makeMove(self, board):
        pass
    
