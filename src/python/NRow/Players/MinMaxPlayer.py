from Players.PlayerController import PlayerController
from Tree import Tree
import Game

class MinMaxPlayer(PlayerController):
    def __init__(self, playerId, gameN, depth, heuristic):
        super().__init__(playerId, gameN, heuristic)
        self.depth = depth

    """
    @param board: the current board
    @return: column integer the player chose
    """
    def makeMove(self, board):
        tree = Tree(board, self.depth, self.playerId)
        self.minMax(self.depth, tree)

        # choose the best move based on the calculated utilities in the game tree
        bestUtility = float("-inf") if self.playerId == 1 else float("inf")
        bestMove = None
        for child in tree.children:
            if self.playerId == 1 and child.utility > bestUtility or self.playerId == 2 and child.utility < bestUtility:
                bestUtility = child.utility
                bestMove = child.lastMove 

        # if there is no best move, just make any valid one
        if bestMove is None:
            bestMove = 0
            while not board.isValid(bestMove):
                bestMove += 1

        self.moveCount += 1
        return bestMove

    """
    Runs the MinMax algorithm and stores the potential utility of each node
    @param depth: how many plies to calculate for
    @param tree: the game tree to use
    @return the optimal utility for this player
    """
    def minMax(self, depth, tree):
        # base case: terminal node
        if depth == 0 or Game.winning(tree.board, self.gameN) != 0:
            utility = self.heuristic.evaluateBoard(tree.playerId, tree.board)
            tree.utility = utility
            return utility
        # maximizer ply
        if tree.playerId == 1:
            value = float("-inf")
            for child in tree.children:
                value = max(value, self.minMax(depth - 1, child))
            tree.utility = value
            return value
        # minimizer ply
        else:
            value = float("inf")
            for child in tree.children:
                value = min(value, self.minMax(depth - 1, child))
            tree.utility = value
            return value 

