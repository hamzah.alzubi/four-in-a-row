from Heuristics.Heuristic import Heuristic
import Game

class SimpleHeuristic(Heuristic):
    def __init__(self, gameN):
        super().__init__(gameN)

    def name(self):
        return "Simple"

    """
    Determines utility of a board state
    """
    def evaluate(self, player, board):
        boardState = board.getBoardState()
        winning = Game.winning(board, self.gameN)
        if winning == player:
            return float("inf")
        elif winning != 0:
            return float("-inf")

        # If not winning or losing, return highest number of claimed squares in a row
        maxInRow = 0
        for r in range(0, board.height):
            for c in range(0, board.width):
                if boardState[r][c] == player:
                    maxInRow = max(maxInRow, 1)
                    for n in range(1, board.height - r):
                        if boardState[r+n][c] == player:
                            maxInRow = max(maxInRow, n + 1)
                        else:
                            break
                    for n in range(1, board.width - c):
                        if boardState[r][c+n] == player:
                            maxInRow = max(maxInRow, n + 1)
                        else:
                            break
                    for n in range(1, min(board.height - r, board.width - c)):
                        if boardState[r+n][c+n] == player:
                            maxInRow = max(maxInRow, n + 1)
                        else:
                            break
                    for n in range(1, min(board.height - r, c)):
                        if boardState[r+n][c-n] == player:
                            maxInRow = max(maxInRow, n + 1)
                        else:
                            break
        return maxInRow
