import numpy as np

class Heuristic():
    def __init__(self, gameN):
        self.gameN = gameN
        self.evalCount = 0

    """
    @return: The amount of times a boardstate was evaluated
    """
    def getEvalCount(self):
        return self.evalCount

    """
    Determines the best column for the next move
    @param player: the player for which to compute the heuristic values
    @param board: the board to evaluate
    @return: column integer
    """
    def getBestAction(self, player, board):
        utilities = self.evalActions(player, board)
        bestAction = 0
        for i in range(0, len(utilities)):
            bestAction = i if utilities[i] > bestAction else bestAction
        return bestAction

    """
    Helper function to determine the utility of each column
    @param player: the player for which to compute the heuristic values
    @param board: the board to evaluate
    @return: array of size boardWidth with utilities
    """
    def evalActions(self, player, board):
        utilities = np.empty(board.width)
        for i in range(0, board.width-1):
            utilities[i] = self.evaluateAction(player, i, board)
        return utilities

    """
    Helper function to assign a utility to an action
    @param player: the player for which to compute the heuristic values
    @param action: the action to evaluate
    @param board: the board to evaluate
    @return: the utility, negative 'infinity' if the move is invalid
    """
    def evaluateAction(self, player, action, board):
        if board.isValid(action):
            self.evalCount += 1
            return self.evaluateBoard(player, board.getNewBoard(action, player))
        return float("-inf")

    """
    Helper function to assign a utility to a board
    @param player: the player for which to compute the heuristic values
    @param board: the board to evaluate
    @return: the utility
    """
    def evaluateBoard(self, player, board):
        self.evalCount += 1
        return self.evaluate(player, board)

    def __str__(self):
        return self.name()

    def name(self):
        pass

    """
    @param player: the player for which to compute the heuristic value
    @param board: the board to evaluate
    @return: heuristic value for the board state
    """
    def evaluate(self, player, board):
        pass
