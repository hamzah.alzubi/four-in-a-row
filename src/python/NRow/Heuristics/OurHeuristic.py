from Heuristics.Heuristic import Heuristic
import Game

class OurHeuristic(Heuristic):
    def __init__(self, gameN):
        super().__init__(gameN)

    def name(self):
        return "Bob"

    """
    Determines utility of a board state
    """
    def evaluate(self, player, board):
        boardState = board.getBoardState()
        winning = Game.winning(board, self.gameN)
        if winning == 1:
            return float("inf")
        elif winning == 2:
            return float("-inf")
        elif winning == -1:
            return float("-inf") if player == 2 else float("inf")

        netInRow = 0 # total number of discs in a row, with extra points for longer chains

        # vertical pass
        for r in range(0, board.height):
            for c in range(0, board.width):
                currPlayer = boardState[r][c]
                if currPlayer > 0:
                    for n in range(1, board.height - r):
                        if boardState[r+n][c] == currPlayer:
                            if currPlayer == 1:
                                netInRow += n
                            elif currPlayer > 0:
                                netInRow -= n
                            boardState[r+n][c] = -1
                        else:
                            break

        # horizontal pass
        boardState = board.getBoardState()
        for r in range(0, board.height):
            for c in range(0, board.width):
                currPlayer = boardState[r][c]
                if currPlayer > 0:
                    for n in range(1, board.width - c):
                        if boardState[r][c+n] == currPlayer:
                            if currPlayer == 1:
                                netInRow += n
                            elif currPlayer > 0:
                                netInRow -= n
                            boardState[r][c+n] = -1
                        else:
                            break
        
        # descending diagonal pass
        boardState = board.getBoardState()
        for r in range(0, board.height):
            for c in range(0, board.width):
                currPlayer = boardState[r][c]
                if currPlayer > 0:
                    for n in range(1, min(board.height - r, board.width - c)):
                        if boardState[r+n][c+n] == currPlayer:
                            if currPlayer == 1:
                                netInRow += n
                            elif currPlayer > 0:
                                netInRow -= n
                            boardState[r+n][c+n] = -1
                        else:
                            break

        # ascending diagonal pass
        boardState = board.getBoardState()
        for r in range(0, board.height):
            for c in range(0, board.width):
                currPlayer = boardState[r][c]
                if currPlayer > 0:
                    for n in range(1, min(board.height - r, c)):
                        if boardState[r+n][c-n] == currPlayer:
                            if currPlayer == 1:
                                netInRow += n
                            elif currPlayer > 0:
                                netInRow -= n
                            boardState[r+n][c-n] = -1
                        else:
                            break


        # favor placing discs towards the middle of the board
        bonus = 0
        boardState = board.getBoardState()
        midR = round(board.height/2)
        midC = round(board.width/2)
        for r in range(0, board.height):
            for c in range(0, board.width):
                if boardState[r][c] > 0:
                    playerMulti = 1 if boardState[r][c] == 1 else -1
                    bonus += playerMulti * (midR - abs(midR - (r+1)) + (board.height %2 == 0 and r > midR)
                        + (midC - abs(midC - (c+1))) + (board.width %2 == 0 and c > midC) - 2)
        bonus /= self.gameN
    
        return netInRow + bonus
