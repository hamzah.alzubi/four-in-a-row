from Board import Board

import numpy as np

class Game:

    """
    Creates a new game
    @param gameN: N in a row required to win
    @param width: Width of the board
    @param height: Height of the board
    @param players: List of players
    """
    def __init__(self, gameN, width, height, players):
        assert (width % 2 != 0), "Board width must be odd!"
        self.gameN = gameN
        self.players = players
        self.gameBoard = Board(width, height)
        self.winner = None

    """
    Starts the game
    @return: the playerId of the winner and the number of evaluations and moves per player
    """
    def startGame(self):
        print("Start game!")
        currentPlayer = 0

        while not self.isOver():
            # turn player can make a move
            self.gameBoard.play(self.players[currentPlayer].makeMove(self.gameBoard), self.players[currentPlayer].playerId)

            # other player's turn now
            currentPlayer = 1 if currentPlayer == 0 else 0 
        
        print(self.gameBoard)
        if self.winner < 0:
            print("Game is a draw!")
        else:
            print("Player ", self.players[int(self.winner-1)], " won!")
        print("Player ", self.players[0], " evaluated a boardstate ", self.players[0].getEvalCount(), " times and made ", self.players[0].getMoveCount(), " moves.")
        print("Player ", self.players[1], " evaluated a boardstate ", self.players[1].getEvalCount(), " times and made ", self.players[1].getMoveCount(), " moves.")
        
        return (self.winner, self.players[0].getEvalCount(), self.players[0].getMoveCount(), self.players[1].getEvalCount(), self.players[1].getMoveCount())

    """
    Determines whether the game is over
    @return: true if game is over
    """
    def isOver(self):
        self.winner = winning(self.gameBoard, self.gameN)
        return self.winner != 0
    
"""
Determines whether a player has won, and if so, which one
@param board: the board to check
@param gameN: N in a row required to win
@return: 1 or 2 if the respective player won, or 0 if neither player has won
"""
def winning(board, gameN):
    boardState = board.getBoardState()
    
    player = None

    #vertical check
    for r in range(0, board.height - (gameN - 1)):
        for c in range(0, board.width):
            if boardState[r][c] != 0:
                player = boardState[r][c]
                for x in range(1, gameN):
                    if boardState[r+x][c] != player:
                        player = 0
                        break
                if player != 0:
                    return player

    #horizontal check
    for r in range (0, board.height):
        for c in range(0, board.width - (gameN - 1)):
            if boardState[r][c] != 0:
                player = boardState[r][c]
                for x in range(1, gameN):
                    if boardState[r][c+x] != player:
                        player = 0
                        break
                if player != 0:
                    return player

    #ascending diagonal check
    for r in range(0, board.height - (gameN - 1)):
        for c in range(board.width - 1, gameN - 2, -1):
            if boardState[r][c] != 0:
                player = boardState[r][c]
                for x in range(1, gameN):
                    if boardState[r+x][c-x] != player:
                        player = 0
                        break
                if player != 0:
                    return player

    #descending diagonal check
    for r in range(0, board.height - (gameN - 1)):
        for c in range(0, board.width - (gameN - 1)):
            if boardState[r][c] != 0:
                player = boardState[r][c]
                for x in range(1, gameN):
                    if boardState[r+x][c+x] != player:
                        player = 0
                        break
                if player != 0:
                    return player
    
    #check for a draw
    for c in range(0, board.width):
        if boardState[0][c] == 0:
            return 0
    return -1
    
