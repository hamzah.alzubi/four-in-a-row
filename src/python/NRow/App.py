#!/usr/bin/python

import argparse

from Heuristics.SimpleHeuristic import SimpleHeuristic
from Heuristics.OurHeuristic import OurHeuristic
from Players.PlayerController import PlayerController
from Players.HumanPlayer import HumanPlayer
from Players.MinMaxPlayer import MinMaxPlayer
from Players.AlphaBetaPlayer import AlphaBetaPlayer
from Game import Game
from Tree import Tree


def main(gameN, boardWidth, boardHeight, player1Type, player1Heuristic, player1Depth, player2Type, player2Heuristic, player2Depth):
    players = getPlayers(gameN, player1Type, player1Heuristic, player1Depth, player2Type, player2Heuristic, player2Depth)

    game = Game(gameN, boardWidth, boardHeight, players)
    return game.startGame()

"""
Determines the players for the game
@param n: number of discs in a row needed to win
@param player1Type: type of player 1
@param player1Heuristic: heuristic player 1 uses
@param player1Depth: how many future states player 1 takes into account
@param player2Type: type of player 2
@param player2Heuristic: heuristic player 2 uses
@param player2Depth: how many future states player 2 takes into account
@return: an array of size 2 with two PlayerControllers
"""
def getPlayers(n, player1Type, player1Heuristic, player1Depth, player2Type, player2Heuristic, player2Depth):
    heuristic1 = None
    if player1Heuristic == "s":
        heuristic1 = SimpleHeuristic(n)
    elif player1Heuristic == "o":
        heuristic1 = OurHeuristic(n)
    heuristic2 = None
    if player2Heuristic == "s":
        heuristic2 = SimpleHeuristic(n)
    elif player2Heuristic == "o":
        heuristic2 = OurHeuristic(n)

    player1 = None
    if player1Type == "h":
        player1 = HumanPlayer(1, n, heuristic1)
    elif player1Type == "mm":
        player1 = MinMaxPlayer(1, n, player1Depth, heuristic1)
    elif player1Type == "ab":
        player1 = AlphaBetaPlayer(1, n, player1Depth, heuristic1)
    player2 = None
    if player2Type == "h":
        player2 = HumanPlayer(2, n, heuristic2)
    elif player2Type == "mm":
        player2 = MinMaxPlayer(2, n, player2Depth, heuristic2)
    elif player2Type == "ab":
        player2 = AlphaBetaPlayer(2, n, player2Depth, heuristic2)

    return [player1, player2]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Four In A Row AI")
    parser.add_argument("-n", required=False, metavar="N", type=int, default=4, help="the number of discs in a row needed to win. default=4")
    parser.add_argument("-width", required=False, metavar="N", type=int, default=7, help="the number of columns in the board. default=7")
    parser.add_argument("-height", required=False, metavar="N", type=int, default=6, help="the number of rows in the board. default=6")
    parser.add_argument("-type1", required=False, metavar="T", type=str.lower, choices={"h", "mm", "ab"}, default="h", help="the type of player 1: h for a human player, mm for a MinMax AI, ab for an AlphaBeta AI. default=h")
    parser.add_argument("-heuristic1", required=False, metavar="H", type=str.lower, choices={"n", "s", "o"}, default="n", help="the heuristic player 1 uses: n for no heuristic, s for a simple heuristic, o for our heuristic, default=n")
    parser.add_argument("-depth1", required=False, metavar="N", type=int, default=4, help="how many future states player 1 takes into account, only valid if player 1 is not a human. default=4")
    parser.add_argument("-type2", required=False, metavar="T", type=str.lower, choices={"h", "mm", "ab"}, default="h", help="the type of player 2: h for a human player, mm for a MinMax AI, ab for an AlphaBeta AI. default=h")
    parser.add_argument("-heuristic2", required=False, metavar="H", type=str.lower, choices={"n", "s", "o"}, default="n", help="the heuristic player 2 uses: n for no heuristic, s for a simple heuristic, o for our heuristic. default=n")
    parser.add_argument("-depth2", required=False, metavar="N", type=int, default=4, help="how many future states player 2 takes into account, only valid if player 1 is not a human.default=4")
    args = parser.parse_args()

    if (args.type1 != "h" and args.heuristic1 == "n") or (args.type2 != "h" and args.heuristic2 == "n"):
        print("You chose a non-human player. Please specify the heuristic they will use.")
        exit()

    main(args.n, args.width, args.height, args.type1, args.heuristic1, args.depth1, args.type2, args.heuristic2, args.depth2)