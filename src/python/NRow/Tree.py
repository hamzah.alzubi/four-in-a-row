from Board import Board

class Tree:
    """
    Create a recursive game tree
    @param board: board stored in the root node
    @param depth: number of plies to generate
    @param playerId: the player that needs to play the next move
    @param lastMove: the last move that was taken in order to arrive at this state
    """
    def __init__(self, board, depth, playerId=1, lastMove=None):
        self.board = board
        self.playerId = playerId
        self.lastMove = lastMove
        self.utility = None
        self.children = []

        # base case
        if depth == 0:
            return
        # for each possible move from this node's state, recursively generate a child
        for i in range(0, self.board.width):
            if self.board.isValid(i):
                child = Tree(self.board.getNewBoard(i, playerId=self.playerId), depth-1, 2 if self.playerId == 1 else 1, lastMove=i)
                self.children.append(child)

    