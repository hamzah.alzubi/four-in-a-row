import numpy as np

class Board:
    """
    Constructor for creating a new empty board, if the parameters width and height are given;
                for cloning a board based on another board, if the parameter other is given;
                for cloning a board based on a boardstate, if the parameter state is given

    @param width
    @param height
    @param other
    @param state
    """
    def __init__(self, width=7, height=6, other=None, state=None):
        if other is not None:
            self.width = other.width
            self.height = other.height
            self.boardState = other.getBoardState()
        elif state is not None:
            self.width = state.shape[1]
            self.height = state.shape[0]
            self.boardState = state
        else:
            self.width = width
            self.height = height
            self.boardState = np.zeros((height, width))
        self.latest = None #tuple containing the position of the last placed disc

    """
    @param c
    @param r
    @return: The value of a certain coordinate in the board
    """
    def getValue(self, c, r):
        return self.boardState[r][c]

    """
    @return: Cloned int array of the board state
    """
    def getBoardState(self):
        return self.boardState.copy()

    """
    Let player playerId make a move in column c 
    @param c
    @param playerId
    @return: true if succeeded
    """
    def play(self, c, playerId):
        for r in range(self.height-1, -1, -1):
            if (self.boardState[r][c] == 0):
                self.boardState[r][c] = playerId
                self.latest = (r,c)
                return True
        return False

    """
    Returns if a move is valid
    @param c: column of the action
    @return: true if spot is not taken yet
    """
    def isValid(self, c):
        return self.getBoardState()[0][c] == 0

    """
    Gets a new board given a player and their action
    @param c: column of the action
    @param playerId: player that takes the action
    @return: a *new* Board object with the resulting state
    """
    def getNewBoard(self, c, playerId):
        currBoardState = self.getBoardState()
        for r in range(self.height-1, -1, -1):
            if (currBoardState[r][c] == 0):
                currBoardState[r][c] = playerId
                return Board(state=currBoardState)
        return Board(state=currBoardState)
    
    """
    Draws a human readable representation of the board
    """
    def __str__(self):
        numberColumn = "|"
        divider = " "
        divider2 = " "
        for c in range(0, self.width):
            divider += "--- "
            divider2 += "=== "
            numberColumn += " " + str(c + 1) + " |"

        output = ""

        for r in range(0, self.height):
            output += "\n" + str(divider) + "\n"
            for c in range(0, self.width):
                node = " "
                if self.boardState[r][c] == 1:
                    node = "X"
                elif self.boardState[r][c] == 2:
                    node = "O"
                # highlight the latest move made
                if self.latest == (r,c):
                    node = "\u0332" + node 
                output += "| " + str(node) + " "
            output += "|"
        output += "\n" + str(divider2) + "\n" + str(numberColumn) + "\n"

        return output